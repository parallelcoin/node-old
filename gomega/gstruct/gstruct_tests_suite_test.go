package gstruct_test

import (
	"testing"

	. "gitlab.com/parallelcoin/node/ginkgo"
	. "gitlab.com/parallelcoin/node/gomega"
)

func Test(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Gstruct Suite")
}
