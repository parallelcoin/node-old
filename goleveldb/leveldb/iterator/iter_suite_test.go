package iterator_test

import (
	"testing"

	"gitlab.com/parallelcoin/node/goleveldb/leveldb/testutil"
)

func TestIterator(t *testing.T) {
	testutil.RunSuite(t, "Iterator Suite")
}
