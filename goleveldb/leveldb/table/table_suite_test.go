package table

import (
	"testing"

	"gitlab.com/parallelcoin/node/goleveldb/leveldb/testutil"
)

func TestTable(t *testing.T) {
	testutil.RunSuite(t, "Table Suite")
}
