package A_test

import (
	. "gitlab.com/parallelcoin/node/ginkgo/integration/_fixtures/watch_fixtures/A"

	. "gitlab.com/parallelcoin/node/ginkgo"
	. "gitlab.com/parallelcoin/node/gomega"
)

var _ = Describe("A", func() {
	It("should do it", func() {
		Ω(DoIt()).Should(Equal("done!"))
	})
})
