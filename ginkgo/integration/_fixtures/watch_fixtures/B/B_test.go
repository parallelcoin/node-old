package B_test

import (
	. "gitlab.com/parallelcoin/node/ginkgo/integration/_fixtures/watch_fixtures/B"

	. "gitlab.com/parallelcoin/node/ginkgo"
	. "gitlab.com/parallelcoin/node/gomega"
)

var _ = Describe("B", func() {
	It("should do it", func() {
		Ω(DoIt()).Should(Equal("done!"))
	})
})
