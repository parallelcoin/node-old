package tmp

import (
	. "gitlab.com/parallelcoin/node/ginkgo"
	. "gitlab.com/parallelcoin/node/gomega"

	"testing"
)

func TestTmp(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Tmp Suite")
}
