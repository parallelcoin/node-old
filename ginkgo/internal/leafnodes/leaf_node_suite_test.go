package leafnodes_test

import (
	. "gitlab.com/parallelcoin/node/ginkgo"
	. "gitlab.com/parallelcoin/node/gomega"

	"testing"
)

func TestLeafNode(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "LeafNode Suite")
}
